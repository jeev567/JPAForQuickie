package com.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.superman.Superhero;
//This project focuses on JPA based Persistence
public class App {
	public static void main(String[] args) {
		
	Superhero s = new Superhero();
	s.setId(5);
	s.setName("Flash");
		
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu1");
	EntityManager em = emf.createEntityManager();
		
	//FETCH
	Superhero a = em.find(Superhero.class,1);
	
	//SAVE
	em.getTransaction().begin();
	em.persist(s);
	em.getTransaction().commit();
	System.out.println(a);
	}
}
